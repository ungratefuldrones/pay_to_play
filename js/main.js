'use strict';
/*
if ( typeof $ === 'undefined' ){
    $ = jQuery;
}
*/

// load stylesheet

  var wamu_style = document.createElement('link'); wamu_style.rel = 'stylesheet';
  // Path to jquery.js file, eg. Google hosted version
  wamu_style.href = config.dataRoot + 'css/style.css';
  document.getElementsByTagName('head')[0].appendChild(wamu_style);


var year = 31556926 * 1000;
var day = 1000 * 60 * 60 * 24;

var ranges = {
     year: year
    ,day: day
};


var app = angular.module('wamu_app',['ui.select','ngRoute','ngSanitize', 'angular-loading-bar', 'ng.deviceDetector', 'angularSoundManager', 'ngTouch']);

app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeBar = true;
     cfpLoadingBarProvider.includeSpinner = false;
     cfpLoadingBarProvider.latencyThreshold = 500;
}]);

app.config(function(uiSelectConfig) {
  uiSelectConfig.theme = 'bootstrap';
});


String.prototype.pluralize = function(count, plural)
{
  if (plural == null)
    plural = this + 's';

  return (count == 1 ? this : plural) 
}

function htmlDecode(input){
  var e = document.createElement('div');
  e.innerHTML = input;
  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function blur_defs(def){

      // blur        
      defs.append('defs')
      .append('filter')
        .attr({
                 'id' : 'blur'
                ,'y'  : '-40%' 
                ,'height'  : '180%'
        })
      .append('feGaussianBlur')
        .attr({
            'in': 'SourceGraphic'
            ,'stdDeviation': '0.5'
        });
}

function marker_defs(defs){

    var marker = defs.append('marker')
        .attr({
                //'viewBox' : "0 0 10 10" 
                'id' : 'contract'
                ,'viewBox' : "0 0 24 30" 
                ,'markerWidth' :"30" 
                ,'markerHeight': "36" 
                ,'refX' : "12"              
                //,'markerWidth': "6" 
                //,'markerHeight': "6"
                //,'orient': "auto"
        });
            
     var marker_active = defs.append('marker')
        .attr({
                //'viewBox' : "0 0 10 10" 
                'id' : 'contract-active'
                ,'viewBox' : "0 0 24 30" 
                ,'markerWidth' :"30" 
                ,'markerHeight': "36" 
                ,'refX' : "12"              
                //,'markerWidth': "6" 
                //,'markerHeight': "6"
                //,'orient': "auto"
        });

                marker.append('g').attr('transform', function(){return 'translate(0 -1028.4)';}).style('pointer-events', 'none').append('rect').attr({
                    'x': "5.687"
                    ,'y':"3.087" 
                    ,'fill':"white"
                    ,'width':"11.896" 
                    ,'height':"9.913"
                    ,'transform':'translate(0 1028.4)'
                });
                
                marker.select('g')
                .append('path')
                .attr({
                     'd':'m12 0c-4.4183 2.3685e-15 -8 3.5817-8 8 0 1.421 0.3816 2.75 1.0312 3.906 0.1079 0.192 0.221 0.381 0.3438 0.563l6.625 11.531 6.625-11.531c0.102-0.151 0.19-0.311 0.281-0.469l0.063-0.094c0.649-1.156 1.031-2.485 1.031-3.906 0-4.4183-3.582-8-8-8zm0 4c2.209 0 4 1.7909 4 4 0 2.209-1.791 4-4 4-2.2091 0-4-1.791-4-4 0-2.2091 1.7909-4 4-4z'
                    //,'fill': '#F37061' //'#5FB783'
                    ,'class': 'contract-emblem'
                    ,'fill':'context-stroke'
                    ,'transform': function(){return "translate(0 1028.4)";}
                })
                ;       
                        

               marker.select('g').append('path')
                .attr({
                     'd':'m12 3c-2.7614 0-5 2.2386-5 5 0 2.761 2.2386 5 5 5 2.761 0 5-2.239 5-5 0-2.7614-2.239-5-5-5zm0 2c1.657 0 3 1.3431 3 3s-1.343 3-3 3-3-1.3431-3-3 1.343-3 3-3z'
                    //,'fill': '#F37061' //'#399C61'
                    ,'class': 'contract-emblem'
                    ,'transform':"translate(0 1028.4)"
                    ,'fill':'context-stroke'
                })
                ;

                marker_active.append('g').attr('transform', function(){return 'translate(0 -1028.4)';}).style('pointer-events', 'none').append('rect').attr({
                    'x': "5.687"
                    ,'y':"3.087" 
                    ,'fill':"white"
                    ,'width':"11.896" 
                    ,'height':"9.913"
                    ,'transform':'translate(0 1028.4)'
                });
                
                marker_active.select('g')
                .append('path')
                .attr({
                     'd':'m12 0c-4.4183 2.3685e-15 -8 3.5817-8 8 0 1.421 0.3816 2.75 1.0312 3.906 0.1079 0.192 0.221 0.381 0.3438 0.563l6.625 11.531 6.625-11.531c0.102-0.151 0.19-0.311 0.281-0.469l0.063-0.094c0.649-1.156 1.031-2.485 1.031-3.906 0-4.4183-3.582-8-8-8zm0 4c2.209 0 4 1.7909 4 4 0 2.209-1.791 4-4 4-2.2091 0-4-1.791-4-4 0-2.2091 1.7909-4 4-4z'
                    //,'fill': '#F37061' //'#5FB783'
                    ,'class': 'contract-emblem-active'
                    ,'fill':'context-stroke'
                    ,'transform': function(){return "translate(0 1028.4)";}
                })
                ;       
                        

               marker_active.select('g').append('path')
                .attr({
                     'd':'m12 3c-2.7614 0-5 2.2386-5 5 0 2.761 2.2386 5 5 5 2.761 0 5-2.239 5-5 0-2.7614-2.239-5-5-5zm0 2c1.657 0 3 1.3431 3 3s-1.343 3-3 3-3-1.3431-3-3 1.343-3 3-3z'
                    //,'fill': '#F37061' //'#399C61'
                    ,'class': 'contract-emblem-active'
                    ,'transform':"translate(0 1028.4)"
                    ,'fill':'context-stroke'
                })
                ;       
}

var table_percent = function(val){
  if ( val > 0.5 ){
    return '<span class="whoa">' + per_cent(val) + '</span>';
  }
  return per_cent(val);
}

var link_official = function(val){
    return "<a ng-click=\"donut_candidate='" + val + "'\">" + val + "</a>";
}

function scrollTopTween(scrollTop) {

  return function() {
    var i = d3.interpolateNumber(this.scrollTop, scrollTop);
    return function(t) { this.scrollTop = i(t); };
 };
}

var cur_form = d3.format("$,.2f");
var per_cent = d3.format("%");
var cur_chart = d3.format("$,.2f");
var cur_brev = d3.format('$.3s');

var date_tick = d3.time.format("%Y-%m-%d");
var date_pretty = d3.time.format("%a %x");      

function ninetyDays(date) {
    var subHalf = d3.time.day.offset(date, -45);
    var addHalf = d3.time.day.offset(date, 45);
    return d3.time.days(subHalf, addHalf, 90)[0];
}
// hasClass
function hasClass(elem, className) {
  return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}
// addClass
function addClass(elem, className) {
    if (!hasClass(elem, className)) {
      elem.className += ' ' + className;
    }
}
// removeClass
function removeClass(elem, className) {
  var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
  if (hasClass(elem, className)) {
        while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
            newClass = newClass.replace(' ' + className + ' ', ' ');
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    }
}
// toggleClass
function toggleClass(elem, className) {
  var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, " " ) + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(" " + className + " ") >= 0 ) {
            newClass = newClass.replace( " " + className + " " , " " );
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    } else {
        elem.className += ' ' + className;
    }
}

app.filter('with', function() {

    return function(items, field) {
        if (typeof items === 'undefined') {return;}
        var result = {};
        //angular.forEach(items, function(value, key) {
            if (items.hasOwnProperty(field)) {
                result = items[field];
          }
        //});
        return result;
    };
});

app.filter('d3_currency', function() {
    return function(items){
     if (typeof items === 'undefined') {return;}
         return cur_brev(items);
    };
});

app.filter('slugify', function() {
    return function(items){
     if (typeof items === 'undefined') {return;}
         return items.toLowerCase().replace(' ', '_');
    };
});

d3.selection.prototype.moveToFront = function() {
  return this.each(function(){
    this.parentNode.appendChild(this);
  });
};


function getDimensions(el){
    if (Array.isArray(el)){
        el = el[0];    
    }
    var style = document.defaultView.getComputedStyle(el);

    return {
         width: parseInt(style.width)
        ,height: parseInt(style.height)
    };
}

function wrap(text, width) {
  text.each(function() {
    var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr("y"),
        dy = parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}

