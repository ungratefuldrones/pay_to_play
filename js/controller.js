"use strict";

// app-wide parent controller
app.controller('AppCtrl', function ($scope, $rootScope, $route,$routeParams, $window, cf, cfpLoadingBar, deviceDetector, $location, $anchorScroll) {

    // init filters
    $rootScope.filters = {};
    $rootScope.audio_playing = false;
    // setup crossfilter factory
	cf.setUp($rootScope);
    $rootScope.stuff = 'total';
    $rootScope.contractorType = "";
    $rootScope.cFilterAccessor = 'candidatename';
    $rootScope.contractno = undefined;

    $rootScope.logos = {
         svg: 'http://static.wamu.org/a/wamu_logo2.svg'
        ,png: 'http://static.wamu.org/g/madden/wamu_hz.png'
    
    };

    $rootScope.wamu_logo = function(){

        return deviceDetector.browser == 'ie' ? $scope.logos.png : $scope.logos.svg;
    };

    $rootScope.candidate_bio = function(){
        if (typeof $rootScope.filters.candidates === 'undefined'){return;}
        return 'views/bios/' + $rootScope.filters.candidates.toLowerCase().replace(/ /g ,'_') + '.html';
    }

    $rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute) {
        $location.hash($routeParams.scrollTo);
        $anchorScroll();  
      });
/*
    $scope.audio_playing = $rootScope.audio_playing;
    $scope.$watch('isPlaying', function(){console.log('playing');});
*/
    $scope.songs = [
            {
                id: 'one',
                title: 'PAYTOPLAY-MADDEN',
                artist: 'WAMU',
                url: 'http://static.wamu.org/g/madden/PAYTOPLAY-MADDEN.mp3'
            }
        ];

    $rootScope.industries= function (){
        return $rootScope.contractor_industry[$rootScope.filters.contractors];
    };

    $rootScope.d3_scroll = function (id, duration) {

        duration = typeof duration !== 'undefined' ? duration : 500;

        if (deviceDetector.raw.browser.firefox || deviceDetector.raw.browser.ie ) {

            d3.select("html").transition().duration(duration)
                .tween("uniquetweenname", scrollTopTween(document.getElementById(id).offsetTop));
        } else {

             d3.select("body").transition().duration(duration)
                .tween("uniquetweenname", scrollTopTween(document.getElementById(id).offsetTop));                       
        }
};

    // table config
    $rootScope.contractor_table = {
        columns : [
                 {contractor: 'Contractor'}
                ,{total_amount: 'Total amount contributed'}
                //,{$contract: 'Total contract award'}
                ,{percent_in_1: 'Percent in 1 yr of contract award'}
                //,{in_range_1: 'No. within 1 year'}
                //,{total_in_range_1: '$ within 1 year'}
                ,{percent_in_2: 'Percent in 2 yrs of contract award'}
                //,{in_range_2: 'No. within 2 years'}
                //,{total_in_range_2: '$ within 2 years'}
            ]

        ,formats :  [
                         {total_amount: 'cur_chart'}
                        //,{$contract: 'cur_chart'}
                        ,{percent_in_1: 'table_percent'}
                        ,{total_in_range_1: 'cur_chart'}
                        ,{percent_in_2: 'table_percent'}
                        ,{total_in_range_2: 'cur_chart'}                                    
                    ]
        ,totals : [
                     'total_text'
                    ,'cur_chart(sum(\'total_amount\'))'
                    //,'cur_chart(sum(\'$contract\'))'
                    //,'sum(\'total_number\')'
                    ,'percentage(\'total_in_range_1\', \'total_amount\')'
                    //,'sum(\'in_range_1\')'
                    //,'cur_chart(sum(\'total_in_range_1\'))'
                    ,'percentage(\'total_in_range_2\', \'total_amount\')'
                    //,'sum(\'in_range_2\')'
                    //,'cur_chart(sum(\'total_in_range_2\'))'
            ]
    };

    $rootScope.incumbent_table = {
        columns : [
                     {incumbent: 'Official'}
                    ,{$total: 'Total amount received from contractors'}
                    //,{total: 'Total No.'}
                    ,{percent_in_1: 'Percent in 1 yr of contract award'}
                    ,{percent_in_2: 'Percent in 2 yrs of contract award'}
                    //,{in_range_1: 'No. within 1 year'}
                    //,{total_in_range_1: '$ within 1 year'}
                    //,{in_range_2: 'No. within 2 years'}
                    //,{total_in_range_2: '$ within 2 years'}
                ]

        ,formats : [
                     {$total: 'cur_chart'}
                    //,{incumbent: 'link_official'}
                    ,{percent_in_1: 'table_percent'}
                    ,{total_in_range_1: 'cur_chart'}
                    ,{percent_in_2: 'table_percent'}
                    ,{total_in_range_2: 'cur_chart'}                                    
                ]
        ,totals : [
                     'total_text'
                    ,'cur_chart(sum(\'$total\'))'
                    //,'sum(\'total\')'
                    ,'percentage(\'total_in_range_1\', \'$total\')'
                    //,'sum(\'in_range_1\')'
                    //,'cur_chart(sum(\'total_in_range_1\'))'
                    ,'percentage(\'total_in_range_2\', \'$total\')'
                    //,'sum(\'in_range_2\')'
                    //,'cur_chart(sum(\'total_in_range_2\'))'
            ]
    };




    $scope.compare = function (prop, desc) {
            desc = typeof desc !== 'undefined' ? desc : true;
            
            if (!!desc) {
                return function (a,b) {
                    if (a[prop] < b[prop])
                     return 1;
                    if (a[prop] > b[prop])
                    return -1;
                    return 0;
                }
            } else {
                return function (a,b) {
                    if (a[prop] < b[prop])
                     return -1;
                    if (a[prop] > b[prop])
                    return 1;
                    return 0;
                }                
            }
    }

    //$scope.load_progress = cfpLoadingBar.status();

    angular.element($window).on('resize', function () { $scope.$apply(); });
    angular.element($window).bind('orientationchange', function () { $scope.$apply(); });

    // Update the rendering of the page.
    var render = function(){
        var renderAction = $route.current.action;
        //var renderPath = renderAction.split( "." );
        //debugger
        //var _name = ($routeParams._name || "");
        var isHome = (renderAction == "home.default");
        var isContractor = (renderAction == "contractors.list");
        var isStory = (renderAction == "story");
        var isOfficial = (renderAction == "officials.list");
        //var isSandbox = (renderAction == "sandbox.list");

        $scope.renderAction = renderAction;
        //$scope.renderPath = renderPath;
        $scope.isHome = isHome;
        $scope.isContractor = isContractor;
        $scope.isStory = isStory;
        $scope.isOfficial = isOfficial;  
        //$scope.isSandbox = isSandbox;        
    
        $scope.tipTouched = false;
    };

    $scope.$on(
        "$routeChangeSuccess",
        function( $currentRoute, $previousRoute ){

            render();
        }
    );

    // when data is loaded broadcast
    $scope.$watchCollection('loaded', function(){
        if (typeof $scope.loaded.contracts === 'undefined' || typeof $scope.loaded.contributions === 'undefined' 
            || typeof $scope.loaded.incumbents === 'undefined' || typeof $scope.loaded.committees === 'undefined'){
                return;d
        }        
        $scope.$broadcast('dataLoaded', {
          someProp: 'Sending you an Object!' 
        });
    });

}); // end AppCtrl 

app.controller('DataCtrl', function ($scope, cf) {

    

});




// controller for various plots
app.controller('MetricsCtrl', function ($scope, cf) {




});


// controller for various plots
app.controller('PlotCtrl', function ($scope,$rootScope, cf) {

    $scope.loaded = false;
    $scope.incumbent_metric = $rootScope.inc_table;
    $scope.contractor_metric = $rootScope.contractor_metric;
    $scope.fundraising_percentage = $rootScope.fundraising_percentage;

    $scope.$on('dataLoaded', function (event, data) {
        $rootScope.incumbent_metric = $scope.inc_table();
        $rootScope.contractor_metric = $scope.contractor_metrics();
        $rootScope.fundraising_percentage = $scope.fundraising_percentage();
        $scope.loaded = true;

    });

    $scope.fundraising_percentage = function() {
        var that = $rootScope;
        if( typeof that.fundraising_percentage === 'undefined' ) {
            var rundown = [];
            var pivot = [];
            var committee_not_found = [];

            that.committees.forEach (function (d) {
                $scope.resetCf();
                cf.getCommittees().filter(d.committeename);
                var contributions = angular.copy(/* cf.getCommittees() */cf.getDate().filterRange([new Date('1/1/2005'), Infinity]).bottom(Infinity));
     
                if (contributions.length > 0 ) {       
                    rundown.push({
                         candidatename: contributions[0].candidatename
                        ,committeename: d.committeename
                        ,contractor_total: d3.sum(contributions, function(e){return e.amount})
                        ,overall_total: d.total
                    });
                } else{
                    committee_not_found.push(d.committeename);
                }
            });
            //debugger
            pivot = d3.nest()
                .key(function(d){return d.candidatename;})
                .rollup(function(d){
                    var contractor_total = d3.sum(d, function(e){return e.contractor_total;})
                        ,overall_total = d3.sum(d, function(e){return e.overall_total;})
                        //,contractor_percentage = contractor_total / overall_total
                        ; 
                    //debugger                   
                    return {
                         contractor_total: contractor_total
                        ,non_contractor_total: overall_total - contractor_total
                        //,contractor_percentage: contractor_percentage
                    };
                })
                .entries(rundown);

            that.fundraising_percentage = pivot;
        }
        return that.fundraising_percentage;

    };


    $scope.inc_table = function() {
        
        var that = $rootScope;

        if( typeof that.inc_table === 'undefined' ) {
            that.incumbent_set = d3.set();
            var rundown = [];
            
                // for each incumbent in spreadsheet
                that.incumbents.forEach(function (d){
                    
                    // if startdate1 and enddate1 for office are valid dates (at least one office has been held)
                    if (d.startdate1 instanceof Date && d.enddate1 instanceof Date ) {
                        var range = [];
                        var  in_range       = d3.set()
                            ,in_range_2     = d3.set()
                            ,year           = 31556926 * 1000 // year in milliseconds
                            ;
                        
                        $scope.resetCf();
                        cf.getCandidates().filter(d.candidatename);
                        var contributions = angular.copy(cf.getDate().filterRange([new Date('1/1/2005'), Infinity]).bottom(Infinity));

                        range.push([d.startdate1, d.enddate1]);
                        
                        if (d.startdate2 instanceof Date && d.enddate2 instanceof Date) {
                            range.push([d.startdate2, d.enddate2]);
                        }
                        var _contracts = [];
                        var contracts = [];
                        var contract_set = d3.set();
                        var contractor_set = d3.set();
                        
                        // get contracts during incumbent's tenure
                        range.forEach(function(e){
                            that.contracts.forEach(function(_d){
                                if( + _d.date >= + e[0] && + _d.date <= + e[1] && ! _contracts.some(function(f){_d.contractno == f.contractno})){
                                    //_contracts.push(_d);
                                    
                                    contractor_set.add(_d.contractor);
                                    contractor_set.add(_d.contractor2);
                                    contractor_set.add(_d.contractor3);
                                }
                            });
                        
                        }); 

                        // add contracts for known contractors
                        for(var i = 0; i < that.contractors.length; i+=3){
                            if( contractor_set.has(that.contractors[i])){
                                contract_set.add(i/3);
                            }
                            if( contractor_set.has(that.contractors[i+1])){
                                contract_set.add(i/3);
                            }
                            if( contractor_set.has(that.contractors[i+2])){
                                contract_set.add(i/3);
                            }
                        }

                        contracts = contract_set.values().map(function(e){return that.contracts[e];})

                        //contracts = _contracts;

                        // for each contract in the list
                        contracts.forEach( function (_d) {
                            
                            // date of the contract
                            var _date = +  _d.date ; 

                            // find qualifying contributions to incumbent
                            contributions.forEach(function ( _e, index ) {
                                
                                // return if contractorname is not in one of the 3 contractor columns of the spreadsheet
                                if ([_d.contractor, _d.contractor2, _d.contractor3].indexOf(_e.contractorname) < 0 ){
                                    return;
                                }

                                // date of the contribution
                                var date = + _e.dateofreceipt;
                                
                                // date of receipt within +/- 1 year of date of the contract -- add to in_range
                                if ( date  >= ( _date - year ) && date  <= ( _date + year ) ){
                                    in_range.add(index);
                                } 
                                // date of receipt within +/- 2 years of date of the contract
                                if ( date  >= ( _date - year * 2 ) && date  <= ( _date + year * 2 ) ){
                                    in_range_2.add(index);
                                }
                            });

                        });
                                                // total of all contributions on or after 1/1/2005
                        var  total              = d3.sum(contributions, function(e){ return e.amount;}) 
                            ,contributions_1    = in_range.values().map(function (e){return contributions[e];})
                            ,_in_range_1        = contributions_1.length                            
                            ,total_1            = d3.sum(contributions_1, function(_e){return _e.amount;})
                            ,contributions_2    = in_range_2.values().map(function (e){return contributions[e];}) 
                            ,_in_range_2        = contributions_2.length
                            ,total_2            = d3.sum(contributions_2, function(_e){return _e.amount;})
                            ;
                        
                        if ( total > 0 ) {
                            rundown.push({
                                'incumbent'             : d.candidatename
                                ,
                                '_in_range'             : in_range  //indices of contributions in range
                                ,
                                '_in_range_2'           : in_range_2
                                ,
                                "contracts"             : contracts  // all qualifying contracts
                                //,
                                //"_contracts"          : _contracts //contracts during incumbent's tenure
                                ,
                                "$total"                : total
                                ,
                                "total"                 : contributions.length
                                ,
                                "percent_in_1"          : total_1 / total
                                ,
                                "in_range_1"            : _in_range_1
                                ,
                                "total_in_range_1"      : total_1
                                ,
                                "percent_in_2"          : total_2 / total 
                                ,
                                "in_range_2"            : _in_range_2
                                ,
                                "total_in_range_2"      : total_2
                            });
                        
                            in_range.values().map(function (e){return contributions[e];}).forEach( function(_d) {
                                that.incumbent_set.add(_d.ID);
                                
                            });
                        }
                    }   
                });

            // rundown.sort(that.compare('percent_in_1'));
            that.inc_table = rundown;
        }
        return that.inc_table;

    }

        $scope.contractor_metrics = function() {
        var that = $rootScope;
        
        if( typeof that.contractor_metrics === 'undefined' ) {

            that.contractor_set = d3.set();

            var rundown = [];
            that.contractor_list.forEach(function(d, i){
                if (!rundown.some(function(e){return e.key == d})) {
                    rundown.push({
                         key: d
                        ,values: {
                                dates: []
                        }      
                    });
                }
                
                rundown[i].total = 0;
                that.contracts.forEach(function(e){

                    if ( [e.contractor,e.contractor2,e.contractor3].indexOf(d) > -1 ) {
                        var factor = 0;
                        [e.contractor,e.contractor2,e.contractor3].forEach(function(d){ if (d.length > 0){++factor;}})
                        rundown[i].total += e.amount/factor;
                        rundown[i].values.dates.push(e.date);
                    }
                });
            });

            var contractsByContractor = d3.nest()
                .key(function(d){return d.contractorname;}).sortKeys(d3.ascending)
                .rollup(function(d){
                    var contractor = rundown.filter(function(_d){ return _d.key == d[0].contractorname })[0];
                    var dates = contractor.values.dates.map(function(_e){return _e;});
                    //debugger
                    if ( typeof dates[0] !== 'undefined' ){
                        var // min_date         = + new Date( range[0][0] )
                            //,max_date     = + new Date( range[0][1] )
                             year            = 31556926 * 1000 // year in milliseconds
                            ,in_range       = d3.set()
                            ,in_range_2     = d3.set()
                            ,all            = d3.set()
                            ;

                            d.forEach(function (i, index){
                                var date = + i.dateofreceipt;
                                dates.forEach( function (_d) {

                                    var _date = + new Date( _d);
                                    // date of receipt within +/- 1 year
                                    if ( date  >= ( _date - year ) && date  <= ( _date + year ) ){
                                        in_range.add(index);
                                    } 
                                    if ( date  >= ( _date - year * 2 ) && date  <= ( _date + year * 2 ) ){
                                        in_range_2.add(index);
                                    }

                                });
                                all.add(index);
                            }); 

                        in_range.values().map(function(e){return d[e];}).forEach(function(_d){
                                that.contractor_set.add(_d.ID);
                                
                        }); 
                        //debugger
                        return { 
                                //"date-range"  : range || undefined
                                //,
                                "total"           : contractor.total
                                //,
                                ,"all"           : all.values().map(function(e){return d[e];})
                                ,"1" : {
                                    //"out-range" : out_range.values().map(function(e){return d[e];})
                                    "in-range"  : in_range.values().map(function(e){return d[e];})
                                }
                                ,"2" : {
                                    //"out-range"   : out_range_2.values().map(function(e){return d[e];})
                                    "in-range"  : in_range_2.values().map(function(e){return d[e];})
                                }

                            };
                    
                    } else {
                        return undefined;
                    }
                })
                .entries(that.contributions.filter(function(d){return d.dateofreceipt >= +new Date('1/1/2005') /*&& 

                                                    [
                                                        'Adrian Fenty'
                                                        ,'Anita Bonds'
                                                        ,'Carol Schwartz'
                                                        ,'David Catania'
                                                        ,'David Grosso'
                                                        ,'Harry Thomas'
                                                        ,'Jack Evans'
                                                        ,'Jim Graham'
                                                        ,'Kenyan McDuffie'
                                                        ,'Kwame Brown'
                                                        ,'Marion Barry'
                                                        ,'Mary M Cheh'
                                                        ,'Michael A Brown'
                                                        ,'Muriel Bowser'
                                                        ,'Phil Mendelson'
                                                        ,'Tommy Wells'
                                                        ,'Vincent Gray'
                                                        ,'Vincent Orange'
                                                        ,'Yvette Alexander'
                                                    ]
                                                    .indexOf(d.candidatename) > -1*/
                }));

                var tabular = [];
                contractsByContractor.forEach(function (b){
                    try{
                            var total_in_range_1    = d3.sum(b.values["1"]["in-range"], function(d){return d.amount;});
                            //var total_out_range_1     = d3.sum(b.values["1"]["out-range"], function(d){return d.amount;}); 
                            var total_all               = d3.sum(b.values.all, function(d){return d.amount});
                            var total_in_range_2    = d3.sum(b.values["2"]["in-range"], function(d){return d.amount;});
                            //var total_out_range_2     = d3.sum(b.values["2"]["out-range"], function(d){return d.amount;}); 
                            //var contract_total        = b.values.total[0];    
                            
                            tabular.push(
                                    {
                                         "contractor"       : b.key
                                        ,'$contract': b.values.total
                                        //,"$contract"  : contract_total 
                                        ,"in_range_1"       : b.values["1"]["in-range"].length
                                        ,"total_in_range_1" : total_in_range_1 
                                        //,"out_range_1"        : b.values["1"]["out-range"].length
                                        //,"total_out_range_1": total_out_range_1
                                        ,"in_range_2"       : b.values["2"]["in-range"].length
                                        ,"total_in_range_2" : total_in_range_2 
                                        //,"out_range_2"        : b.values["2"]["out-range"].length
                                        //,"total_out_range_2": total_out_range_2
                                        ,"percent_in_1"     : total_in_range_1 / total_all
                                        ,"percent_in_2"     : total_in_range_2 / total_all 
                                        ,"total_amount"     : total_all
                                        ,"total_number"     : b.values.all.length
                                    });


                        }
                    catch (e) {
                            tabular.push(
                                    {
                                         "contractor"       : b.key
                                        //,"$contract"  : contract_total 
                                        ,"in_range_1"       : ''
                                        ,"total_in_range_1" : ''
                                        //,"out_range_1"        : ''
                                        //,"total_out_range_1": ''
                                        ,"in_range_2"       : ''
                                        ,"total_in_range_2" : ''
                                        //,"out_range_2"        : ''
                                        //,"total_out_range_2": ''
                                        ,"percent_in_1"     : ''
                                        ,"percent_in_2"     : ''
                                    });
                        }



                }); 

            //tabular.sort(that.compare('total_in_range_1'));
            that.contractor_metrics = tabular;
        } // end if

        return that.contractor_metrics;

    } // end contractor metric

    $scope.date_pretty = d3.time.format("%a %x");  

     $scope.resetCf = function (){
        cf.getContractors().filter();
        cf.getCommittees().filter();
        cf.getCandidates().filter();
        cf.getDate().filter();
    };

    $scope.colors = {
        // reds
          'red1': '#ff4500'
        , 'red2': '#ff6f39'
        , 'red3': '#FF8E64'

        //grays
        , 'gray1': '#dfdfdf'
        , 'gray2': '#888'
        , 'gray3': '#dcdcdc'

        // blacks
        , 'black1': '#222'
        
        // orange
        , 'orange1': '#FF7A00'

        // teals
        , 'teal1': '#008194'
        , 'teal2':'#004F5B'
        , 'teal3': '#4BABC2'

    };

    $scope.filtered_contracts = function () {
        var contracts = d3.nest()
            .key(function(d) 
                { 
                    return [d.contractor, d.contractor2, d.contractor3].indexOf($scope.i.trim()) > -1 ? $scope.i.trim() : undefined;  })//.sortKeys(d3.ascending)
            //.key(function(d) { return d.contractorname; })
            .rollup (function(d) { 
                
                return {
                     "total" : d3.sum(d, function(e){ return parseFloat(e.amount); })
                    //,"date-range" : d3.extent(d.map(function(e){ return e.date;}))
                    ,"dates" : d.map(function (e) { 

                        return { 
                              amount: e.amount
                            , date:new Date(e.date)
                           // , notes: e.notes
                            , ceilingcontract: e.ceilingcontract
                            , industry: e.industry
                            , pdflink: e.pdflink
                           // , legislationsummary: e.legislationsummary
                            , contractno: e.contractno
                        };
                    })
                };
                
            })
            .entries($scope.$parent.contracts); //.map(that.contracts, d3.map);

        return contracts.filter(function(d){return d.key == $scope.i;});
    }
}); // end PlotCtrl