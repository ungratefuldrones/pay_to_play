"use strict";

app.config( ['$routeProvider', 
    function( $routeProvider ){

        $routeProvider
             .when(
                "/officials",
                {
                    action: "officials.list"
                }
            )

            .when(
                "/",
                {
                    redirectTo: "/officials"
                }
            )
            .when(
                "/story",
                {

                    action: "story.main"
                }
            )
            .when(
                "/contractors",
                {
                    action: "contractors.list"
                }
            )

           /*
            .when(
                "/incumbents/:_name",
                {
                    action: "incumbents.list"
                }
            )*/
            /*.when(
                "/reset/:_name",
                {
                    action: "reset.item"
                }
            )*/
            .otherwise(
                {
                    redirectTo: "/"
                }
            )
        ;
        //$locationProvider.html5Mode(true);
        //$locationProvider.hashPrefix('!');

    }]
);