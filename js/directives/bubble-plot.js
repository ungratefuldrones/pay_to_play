"use strict";

app.directive('bubblePlot', ['cf', 'deviceDetector', function(cf, deviceDetector) {
    return {
        restrict: 'E',
        replace: true,
        template: '<svg ng-transclude ></svg>',
        transclude: true,
        scope: {
            val: '=',
            i: '@',
            zAccessor: '@'
        },
        link: link,
        controller: 'PlotCtrl'
    };

    function link(scope, element, attrs) {
            var margin = {
                top: 20,
                right: 20,
                bottom: 20,
                left: 120
            };
            var rundown,
                min_date,
                max_date,
                contracts,
                x_range,
                z_range,
                x_extent,
                z_extent,
                el = d3.select(element[0]),
                _svg = el,
                svg = _svg.append('g'),
                width,
                height,
                zAccessor,
                m1 = 0;

            //var tipTouched = false;
            var highlightRange;
            if (!attrs.hasOwnProperty('highlightRange')) {
                highlightRange = {
                    1: 'year'
                };
            } else {
                highlightRange = scope.$eval(attrs.highlightRange);
            }
            var dotSize,markerSize;
            // set touchable sizes if mobile
            if (deviceDetector.device != 'unknown'){
            	dotSize = [5, /*y.rangeBand()/2*/ 12];
            	markerSize = 20;
            } else {
            	dotSize = [3,12];
            	markerSize = 10;
            }


            var clipPath = 'clip' + scope.$id;
            var didInit = false;
            var contractno = undefined;

            var rScale = d3.scale.linear();

            var c1 = function(d) {
                return d % 2 === 0 ? 'rgba(0,0,0,0.0)' : 'gainsboro';
            };
            var c = function(d) {
                return d % 2 === 0 ? '#242900' : '#242900';
            }; //d3.scale.linear().domain([0, 5, attrs.top || 10 ]).range(['#F0881A', '#13B048','#243BA4']);


            var touchTip = d3.tip().attr('class', 'd3-tip touch').offset([0, 0]).html(function(d, i) {
                return 'Double-tap or pinch to zoom.';
            });

            var tip = d3.tip().attr('class', 'd3-tip').offset([0, 0]).direction(function() {
                return width - d3.event.pageX < width / 2 ? 'w' : 'e';
            }).html(
                function(d, i) {
                    //console.log(JSON.stringify(d.records)); 
                    return 'Week of ' + date_pretty(new Date(d.key)) + '' + '<br/><span class="tooltip-contrib">' + cur_brev(d.values.total) + '</span>' + '<br/>' + d.values.contributions + ' contribution'.pluralize(d.values.contributions);
                }
            );
            //var tip = d3.tip().attr('class', 'd3-tip').html(function(d,i) { return '<u>' + d.candidatename + '</u><br/><br/>' + scope.date_pretty(new Date(d.key)) + '<br/>' +  cur_form(d.values.total) + '<br/>' + d.values.contributions + ' contributions'; });
            var cTip = d3.tip().attr('class', 'd3-tip').offset([0, 0]).direction(function() {
                return width - d3.event.pageX < width / 2 ? 'w' : 'e';
            }).html(function(d, i) {
            	var ceil = d.ceilingcontract == 'yes' ? '*': '';
                return '<span class="dv-badge">' + d.contractno + '</span><span class="dv-badge">' + d.industry + '</span><span class="dv-badge">' + date_pretty(d.date) + '</span>' 
                + '<br/><br/><span class="tooltip-contract">' + cur_brev(d.amount) + '</span>&nbsp;&nbsp;contract award ' + ceil 
                + '<br/><span class="tooltip-contrib">' + cur_brev(d.contrib_total) + '</span>&nbsp;&nbsp;total contributed in ' + d3.keys(highlightRange)[0] + ' ' + d3.values(highlightRange)[0] + ' range' 
                + '<br/><br/><span class="tt-link"><span class="glyphicon glyphicon-file"></span> <a target="_blank" href="' + d.pdflink + '">Source</a></span>'

                ;
            });
            var w = getDimensions(_svg[0]).width;
            var h = getDimensions(_svg[0]).height;
            var _h = angular.copy(h);

            var y = d3.scale.ordinal();
            var x = d3.time.scale();

            var defs = svg.append('defs');
            var yAxis = d3.svg.axis()
                .scale(y)
                .orient('left');

            var xAxis = d3.svg.axis()
                .scale(x)
                .orient('top');

            var marker = typeof attrs.marker !== 'undefined' ? scope.$eval(attrs.marker) : undefined;

            // do the g's
            _svg.attr('class', 'bubble-plot');
            var row = svg.append('g').attr('class', 'row-bg');
            var xAxisG = svg.append('g').attr('class', 'x axis');
            var yAxisG = svg.append('g').attr('class', 'y axis');
            var base = svg.append('g').attr('class', 'zoom');
            var markerG = svg.append('g').attr('class', 'marker');
            var rowContainer = svg.append('g').attr('class', 'row-container');
            //var rowHighlight = svg.append('g').attr('class', 'row-container');
            var markerGUI = svg.append('g').attr('class', 'marker');
            var yAccessor = typeof attrs.yAccessor !== 'undefined' ? attrs.yAccessor : 'candidatename';

            var zoom = d3.behavior.zoom();

            // disable y axis ticks
            yAxis.tickFormat(function(d) {
                return '';
            });
            //var rects  = svg.append('g').attr('class', 'rects').selectAll("g.rect");

            // ensure sure contributions are loaded
            //this might be more efficient than $watch
            scope.$watchCollection('val', init);
            scope.$watch('i', init);
            scope.$watch('zAccessor', init);

            scope.$watch(function() {
                w = getDimensions(_svg[0]).width;
                h = getDimensions(_svg[0]).height;

                return w + h;
            }, resize, true);

            // remove tooltips on destroy
            scope.$on("$destroy", function() {
                cTip.hide();
                tip.hide();
            });

            function crossfilter_init(highlight_range) {
                scope.resetCf();
                cf.getDate().filterRange([new Date('01/01/2005'), Infinity]);
                cf.getContractors().filter(scope.i);
                if (typeof highlight_range !== 'undefined') {
                    cf.getDate().filterRange([highlight_range[0], highlight_range[1]])
                }
            }

            // to controller?
            function init() {
                if (typeof scope.val.contracts === 'undefined' || typeof scope.val.contributions === 'undefined' || typeof scope.val.incumbents === 'undefined') {
                    return;
                }
                didInit = false;
                zAccessor = scope.zAccessor || 'total';
                crossfilter_init();

                function compVal(a, b) {
                    return d3.sum(b.values, function(e) {
                        return e.values[zAccessor];
                    }) < d3.sum(a.values, function(e) {
                        return e.values[zAccessor];
                    }) ? -1 : d3.sum(b.values, function(e) {
                        return e.values[zAccessor];
                    }) > d3.sum(a.values, function(e) {
                        return e.values[zAccessor];
                    }) ? 1 : 0;
                }

                contracts = scope.filtered_contracts();

                rundown = d3.nest()
                    .key(function(d) {
                        return d.candidatename;
                    })
                    //.sortValues(compVal)
                    .key(function(d) {
                        return d3.time.week(new Date(d.dateofreceipt));
                    })
                    .sortKeys(function(a, b) {
                        return +new Date(b) < +new Date(a) ? -1 : +new Date(b) > +new Date(a) ? 1 : 0;
                    })
                    //.key(function(d) { return d.contractorname; })
                    .rollup(function(d) {
                        return {
                            "total": d3.sum(d, function(e) {
                                return parseFloat(e.amount);
                            }),
                            "contributions": d.length
                        };
                    })
                    .entries(cf.getContractors().top(Infinity));

                rundown = rundown.sort(compVal).slice(0, attrs.top);

                //debugger
                /*
					if (yAccessor == 'contractorname'){
						cf.getContractors().filter(attrs.i);

						totals = angular.copy(cf.getCandidateRaisedSum().top(top));


					} else {
						if (yAccessor == 'committeename'){
							cf.getCommittees().filter(attrs.i);
						} else {
							cf.getCandidates().filter(attrs.i);
						}

						totals = angular.copy(cf.getContractorContribSum().top(top));

					}*/
                zoom
                    .scaleExtent([1.0, 20.0])
                    .on("zoom", update);

                if (typeof rundown !== 'undefined' && typeof contracts !== 'undefined') {

                    resize();
                }

                didInit = !didInit;
            }

            function highlight(d) {
                cTip.hide();
                d3.selectAll('#c-' + d.contractno).attr('marker-start', 'url(#contract-active)').moveToFront();
                contractno = d.contractno;
                var con_date = +new Date(d.date);

                var interval = parseInt(d3.keys(highlightRange)[0]) * ranges[d3.values(highlightRange)[0]];

                d3.selectAll('.bubble-node').filter(function(e, i) {
                        var contrib_date = +new Date(e.key);
                        return contrib_date >= con_date - interval && contrib_date <= con_date + interval;
                    })
                    .classed('highlight', true)
                    //.
                    .transition()
                    .style('fill', '#004F5B');

                d3.selectAll('.bubble-node').filter(function(e, i) {
                        var contrib_date = +new Date(e.key);
                        return contrib_date < con_date - interval || contrib_date > con_date + interval;
                    })
                    .classed('disabled', true)
                    //.
                    .transition()
                    .style('fill', 'gainsboro');

                d3.selectAll('.bubble-label').filter(function(e, i) {
                        return this.parentNode.querySelectorAll('.highlight').length == 0;
                    })
                    .classed('active', true)
                    .transition()
                    .style('fill', 'gainsboro');

                crossfilter_init([new Date(con_date - interval), new Date(con_date + interval)]);

                d.contrib_total = d3.sum(cf.getContractors().top(Infinity), function(d) {
                    return d.amount
                });
				d.contrib_count = cf.getContractors().top(Infinity).highlight;
                cTip.show(d);

            }

            function rm_highlight(d) {
                cTip.hide();
                d3.selectAll('#c-' + contractno).attr('marker-start', 'url(#contract)');
                contractno = undefined;

                d3.selectAll('.bubble-node.highlight')
                    .classed('highlight', false)
                    //.style('fill', '#004F5B')
                    //.transition()
                    .style('fill', '#888');

                d3.selectAll('.bubble-node.disabled').classed('disabled', false).transition().style('fill', '#888');
                d3.selectAll('.bubble-label.active').classed('active', false).transition().style('fill', 'black');

            }


            function resize() {
                if (typeof rundown === 'undefined' || typeof contracts === 'undefined') {
                    return;
                }
                if (rundown.length < 10) {
                    h = rundown.length * 50 > 140 ? rundown.length * 50 : 140;

                } else {
                    h = angular.copy(_h);
                }
                _svg.style('height', h);

                width = w - margin.left - margin.right;
                height = h - margin.bottom;
                _svg.attr({
                    width: width + margin.left + margin.right,
                    height: height + margin.top + margin.bottom
                });
                svg.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
                x.range([0, width]);
                y.range([height, 0]);
                xAxis.ticks(Math.max(width / 100, 2));
                xAxis.tickSize(-height, 5, -3);
                yAxis.tickSize(-width, 5, 5);
                xAxisG.attr('transform', "translate(0," + margin.top + ")");
                yAxisG.attr('transform', 'translate(0,0)');
                y.rangeRoundBands([0, height], 1.5);

                z_range = rundown.map(function(d) {
                    return d.values.map(function(e) {
                        return e.values[zAccessor];
                    })
                });
                x_range = rundown.map(function(d) {
                    return d.values.map(function(e) {
                        return new Date(e.key);
                    })
                });

                var contrib_min = d3.min(x_range, function(d) {
                    return d3.min(d);
                });

                var contrib_max = d3.max(x_range, function(d) {
                    return d3.max(d);
                });

                if (typeof contracts[0] !== 'undefined') {
                    // contract min date
                    var contract_min = d3.min(contracts[0].values.dates, function(d) {
                        return d.date;
                    });
                    //contract max date
                    var contract_max = d3.max(contracts[0].values.dates, function(d) {
                        return d.date;
                    });
                    min_date = contract_min < contrib_min ? contract_min : contrib_min;
                    max_date = contract_max > contrib_max ? contract_max : contrib_max;
                } else {
                    min_date = contrib_min;
                    max_date = contrib_max;
                }

                x_extent = [min_date, max_date];
                z_extent = [d3.min(z_range.map(function(d) {
                    return d3.min(d)
                })), d3.max(z_range.map(function(d) {
                    return d3.max(d)
                }))];

                x.domain(x_extent); //.nice();
                y.domain(rundown.map(function(d) {
                    return d.key;
                }));

                rScale
                    .domain(z_extent)
                    .range(dotSize);

                base.selectAll('rect').remove();
                base.append("rect")
                    .attr({
                        'class': 'pane',
                        'width': width,
                        "height": height
                    })
                    .on("touchstart", function(d) {
                        d3.event.preventDefault();
                        if (!scope.$parent.$parent.tipTouched) {
                            scope.$parent.$parent.tipTouched = true;
                            touchTip.show(d);
                            d3.selectAll(".d3-tip.touch")
                                .style('opacity', 1)

                            .transition()
                                .delay(3000)
                                .duration(600)
                                .style("opacity", 0)
                                .style('pointer-events', 'none');
                        }
                    })
                    .on('touchend', function(d) { 
							//d3.event.stopImmediatePropagation();
							//d3.event.stopPropagation();
							//d3.event.preventDefault();
							rm_highlight();
							//cTip.hide();
							tip.hide();
					})
					.on('mouseenter', function(d,i){rm_highlight();tip.hide();cTip.hide();})
                    .call(zoom);
                zoom.x(x);

                if (typeof scope.val.contracts === 'undefined' || typeof scope.val.contributions === 'undefined' || typeof scope.val.incumbents === 'undefined') {
                    return;
                }
                update();
            }

            function update() {
                    svg.call(cTip);
                    svg.call(tip);

                    svg.call(touchTip);
                    tip.hide();
                    cTip.hide();

                    defs.selectAll('#' + clipPath).remove();

                    defs.append("clipPath")
                        .attr("id", clipPath)
                        .attr('transform', 'translate(-5, -5)')
                        .append("rect")
                        .attr("height", height + 10)
                        .attr("width", width + 30);

                    var tx, ty;
                    tx = zoom.translate()[0];
                    ty = zoom.translate()[1];
                    tx = Math.min(1, Math.max(tx, (width) - Math.ceil(x(max_date) - x(min_date)), (width) - Math.ceil(x(max_date) - x(min_date)) * zoom.scale()));
                    zoom.translate([tx, ty]);

                    rowContainer.attr('transform', 'translate(0, 25)');
                    //rowHighlight.attr('transform', 'translate(0, 25)');
                    row.attr('transform', 'translate(0, 25)');

                    svg.selectAll('g.row').remove();
                    svg.selectAll('g.row-bg').selectAll('rect').remove();
                    rundown.forEach(function(el, i) {
                        var g = rowContainer.append("g").attr("class", "row")
                            .attr('height', Math.abs(y.rangeBand()));

                        if (i % 2 == 0) {
                            row.append('rect')
                                .attr('height', Math.abs(y.rangeBand()) * 2)
                                .attr('width', function(d) {
                                    return width + margin.left;
                                })
                                .attr('x', function(d) {
                                    return -margin.left;
                                })
                                .attr('y', function(d) {
                                    return y(el['key']) - Math.abs(y.rangeBand()) * 1.5
                                });
                        }

                        var circles = g.selectAll("circle")
                            .data(el['values'])
                            .enter()
                            .append("circle");
                        /*
						var text = g.selectAll("text")
							.data(el['values'])
							.enter()
							.append("text");
						*/

                        circles
                            .attr("cx", function(d, i) {
                                d.candidatename = el['key'];
                                return x(new Date(d.key));
                            })
                            .attr("cy", y(el['key']) - Math.abs(y.rangeBand()) / 2)
                            .attr("r", function(d) {
                                return rScale(d.values[zAccessor]);
                            })
                            //.style("fill", function(d) { return c(i)/*scope.colors.black1*/; })
                            .attr('class', 'bubble-node')
                            .style('pointer-events', 'all')
                            .attr("clip-path", function(d) {
                                return "url(#" + clipPath + ")"
                            })
                            //.style("stroke", function(d) { return d3.hcl(c(i)).brighter(4);})
                            //.style('opacity', 0)
                            .on('mouseover', tip.show)
                            .on('mouseout', tip.hide)
                            .on('touchstart', tip.show);
                        /*
						if (!didInit){
								circles.style("opacity",0.0)						
									  
								  	.transition()
								  	//.duration(1000)
	  							   .style("opacity",1.0);
						}
						*/

                        g.append("text")
                            .attr('class', 'bubble-label')
                            .attr("y", y(el['key']) - Math.abs(y.rangeBand()) / 4)
                            .attr("height", Math.abs(y.rangeBand()))

                        .attr("x", -6)
                            .attr('text-anchor', 'end')
                            //.attr("class","label")
                            .text(el['key']);
                        //.style("fill", function(d) { return /*d3.hcl(*/c(i)/*).darker(2)*//*scope.colors.black1*/; })
                        //.on("mouseover", mouseover)
                        //.on("mouseout", mouseout);
                        ;
                    });

                    //	Remove markers
                    markerG.selectAll("line").remove();
                    markerG.selectAll("circle").remove();
                    markerGUI.selectAll("circle").remove();
                    if (typeof contracts[0] !== 'undefined') {
                        // Add markers
                        markerGUI.selectAll("marker-line")
                            .data(contracts[0].values.dates)
                            .enter()
                            .append('circle')
                            .attr({
                                'r': markerSize,
                                'cx': function(d) {
                                    return x(d.date);
                                },
                                'cy': 30
                            })
                            .attr('class', 'marker-ui')
                            .on('mousedown', function(d) {

                                d3.event.stopImmediatePropagation();
                                d3.event.stopPropagation();
                                d3.event.preventDefault();
                                //cTip.show(d);
                                if (contractno == d.contractno) {
                                    rm_highlight()
                                } else {
                                    //console.log('foot');
                                    rm_highlight();
                                    highlight(d);
                                }
                            })
                        	.on('touchstart', function(d) {
                                d3.event.stopImmediatePropagation();
                                d3.event.stopPropagation();
                                d3.event.preventDefault();
                                if (contractno == d.contractno) {
                                    rm_highlight()
                                } else {
                                    rm_highlight();
                                    highlight(d);
                                }
                            })

                            ;

                        markerG.selectAll("marker-line")
                            .data(contracts[0].values.dates)
                            .enter()
                            .append("line")
                            .attr('id', function(d) {
                                return 'c-' + d.contractno;
                            })
                            .attr("x1", function(d) {
                                return x(d.date);
                            })
                            .attr("x2", function(d) {
                                return x(d.date);
                            })
                            .attr("y1", 22)
                            .attr("y2", height)
                            .attr('marker-start', 'url(#contract)')
                            .attr('class', 'marker-line')
                            .attr("clip-path", function(d) {
                                return "url(#" + clipPath + ")"
                            });
                    }

                    xAxisG.call(xAxis).selectAll("text")
                        //.style("text-anchor", "end")
                    ;

                    yAxisG.call(yAxis);
                } // end update
        } // end link
        //end return 
}]); // end app.directive
