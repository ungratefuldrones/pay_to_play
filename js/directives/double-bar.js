"use strict";

	app.directive('doubleBar', ['cf', function (cf) {
		return {
			 restrict: 'E'
			,replace: true
			,template: '<svg ng-transclude ></svg>'
			,transclude: true
			,scope: 
				{
					val: '=', i: "@"
				}
			,
			link: link
			,
			controller: 'PlotCtrl'
		};
			function link (scope, element, attrs) {
				var el = d3.select(element[0]);

				var margin = scope.$eval(attrs.margins) || {top: 20, right: 30, bottom: 20, left: 120};
				
				// axes 
				var x1, x2, y1, y2;

				var _svg = el;
				var svg = _svg.append('g');
				
				var w, h, width, height;
				
				// ensure sure contributions are loaded
				scope.$watchCollection('val', init);
				scope.$watch('i', init);
				
			    scope.$watch(function(){
			      w = getDimensions(_svg[0]).width;  
			      h = getDimensions(_svg[0]).height;

			      return w + h;
			    }, resize, true);


				function init(){

				}

				function resize(){

				}

				function update(){

				}


			} // end link
		} 
	]); // end directive 