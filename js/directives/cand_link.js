app.directive(
    'clickMe',
    ['deviceDetector', function (deviceDetector) {
        return {
           // restrict: 'E',
            //template : '<div>Click me !</div>',
            //replace : true,
            link : function (scope, element, attrs) {
           

                function on_click(accessor, id){

                    scope.$root.filters[accessor] = htmlDecode(element[0].innerHTML);
                    scope.$apply();
                    scope.$root.d3_scroll(id, 500);

                }
                
                if ( attrs['class'] == 'incumbent' ) { 
                    element.bind('mousedown', function(){ on_click('candidates', 'official-profile');}); 
                } else if ( attrs['class'] == 'contractor' ){
                    element.bind('mousedown', function(){ on_click('contractors', 'contractor-graphics');});                    
                }

            },
        };
    }
]);