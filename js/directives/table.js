"use strict";

app.directive('wamuTable', ['cf', '$compile',  function (cf, $compile) {
	return {
		restrict: 'E'
		,scope: 
			{
				columns: '=', sortBy: '@', data: "=", totals: '=', formats: '='
			}
		,controller: 'PlotCtrl'
		,replace: true
		,template: '<div ng-transclude ></div>'
		,transclude: true
		,link: function (scope, element, attrs) {

			var  columns = scope.columns
				,total_text = 'Total'
				,totals_parsed = []
				,el = d3.select(element[0])
				,wrapper_class = attrs['class'] || ''
				;

			el.attr('class', wrapper_class/* + " table-responsive"*/);			
			
			scope.sort = {};
			scope.sort.column = scope.sortBy;
			scope.$watchCollection('loaded' , function() { tableSort(); });


			function init(){;}

		    function tableSort(prop){
	            if(typeof scope.data === 'undefined') return;

	            scope.sort.desc = typeof scope.sort.desc !== 'undefined' ? !scope.sort.desc : true ;
	            scope.sort.column = typeof prop === 'undefined' ? scope.sort.column : prop;

	            scope.data.sort(scope.$parent.compare(scope.sort.column, scope.sort.desc));
	            update(); 
		    }

		    function percentage(dividend, divisor){
		    	return per_cent( sum(dividend) / sum(divisor) );
		    }
		    
		    function sum(prop){
		    	return d3.sum(scope.data, function(d){return d[prop];});
		    }

			function update() {
				// remove table
				el.selectAll('table').remove();

			    var table = el.append("table")
			            .attr("class", ""),
			        thead = table.append("thead"),
			        tbody = table.append("tbody");

			    // append the header row
			    thead.append("tr")
			        .selectAll("th")
			        .data(columns)
			        .enter()
			        .append("th")
			        .text(function(column) { return d3.values(column)[0]; })
			        .on(
			        	"click", function(column,i){
			        		scope.sortBy = d3.keys(column)[0];
			        		return tableSort(d3.keys(column)[0]);
			        	}
			        )
			        .attr(
			        	"class", function (column, i){
			        		if ( d3.keys(column)[0] == scope.sortBy ){
			        			return d3.keys(column)[0] + ' sorted ' + scope.sort.desc ;
			        		} else {
			        			return d3.keys(column)[0];
			        		}
			        	}
			        );

			    // create a row for each object in the data
			    var rows = tbody.selectAll("tr")
			        .data(scope.data)
			        .enter()
			        .append("tr");

			    // create a cell in each row for each column
			    var cells = rows.selectAll("td")
			        .data(function(row) {
			            return columns.map(function(column) {
			                return {column: d3.keys(column)[0], value: row[d3.keys(column)[0]], title: d3.values(column)[0]};
			            });
			        })
			        .enter()
			        .append("td")
			        .attr({

			        	 "class": function(d){ return d.column;}
			        	,'data-title': function(d){ return d.title;}
			        	,'click-me': ''
			        	})
			        .html(function(d) { 

			            	if ( d.value === '' ){
			            		return '&mdash;'
			            	// apply formatting	
			            	} else if ( typeof scope.formats !== 'undefined' && scope.formats.filter(function(e){return e[d.column]}).length > 0 ) {
			            		return eval(scope.formats.filter(function(e){return e[d.column]})[0][d.column])(d.value);
			            	// or no formatting
			            	} else {
			            		return d.value;
			            	}
			        })
			        //.attr('data-toggle', function (d,i){return i == 0 ? 'tooltip': undefined;})
			        .attr('title', function (d,i){return i == 0 ? 'Click to explore the data': undefined;})
			        ;
				if ( typeof scope.totals !== 'undefined' ) {
					
					// evaluate totals formulae 
	 			  	scope.totals.forEach(function(d,i){
	 			   		totals_parsed[i] = eval(d);			
	 			   	});
	 			  	// append totals to table footer
	 			   	table.append('tfoot')
	 			   		.append('tr')
	 			   		.attr('class', 'total')
	 			   		.selectAll('td')
	 			   		.data(totals_parsed)
	 			   		.enter()
	 			   		.append('td')
	 			   		.text(function(d){return d;});
 			   	}
		   	$compile(element.contents())(scope);
			} // update	
		} // link
	} // return 
}]);