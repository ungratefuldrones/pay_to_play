var gulp = require('gulp')
	,gutil = require('gulp-util')
	,concat = require('gulp-concat')
    ,rename = require('gulp-rename')
    ,Download = require("download")
    ,sass = require('gulp-sass')
    ,prefix = require('gulp-autoprefixer')
	,jsmin = require('gulp-jsmin')
    ,request = require('request')
    ,fs = require('fs')
    ,converter = require('json-2-csv')
    ,d3 = require("d3")
    ,minifyCSS = require('gulp-minify-css');
	;

var base_url = 'https://spreadsheets.google.com/feeds/list/{{KEY}}/default/public/values?alt=json-in-script';

require('./js/LZW');

var getJsonFromJsonP = function (url, callback) {
    request(url, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var jsonpData = body;
        var json;
        //if you don't know for sure that you are getting jsonp, then i'd do something like this
        try
        {
           json = JSON.parse(jsonpData);
        }
        catch(e)
        {
            var startPos = jsonpData.indexOf('({');
            var endPos = jsonpData.indexOf('})');
            var jsonString = jsonpData.substring(startPos+1, endPos+1);
            json = JSON.parse(jsonString);
        }
        callback(null, json);
      } else {
        callback(error);
      }
    })
}

function parse_currency(str){
    return parseFloat( str.replace(/,/g,'').replace('$','' ) );
}

contributions = [];

var dl_stuff = [ 
    {
         'key': '1qzb6olVlfBLuVEbkKhMOhHCub9CfOApmDoJLdftJle4'
        ,'callback': function (err, data) {
                    if ( typeof data !== 'undefined' ) {
                        var   parsed = []
                            , csv_parsed =[]
                         ;

                        data.feed.entry.forEach (function (d) {
                            if ( + new Date(d['gsx$dateofreceipt']['$t'].trim()) > + new Date('1/1/2005')) {
                                parsed.push ({
                                    ID:                 d['gsx$ss']['$t'].trim()
                                    ,contractorname:     d['gsx$contractorname']['$t'].trim()
                                    ,committeename:      d['gsx$committeename']['$t'].trim()
                                    ,candidatename:      d['gsx$candidatename']['$t'].trim()
                                    //,contributorname:    d['gsx$contributorname']['$t'].trim()
                                    //address:            d['gsx$address']['$t'].trim(),
                                    //city:               d['gsx$city']['$t'].trim(),
                                    //state:              d['gsx$state']['$t'].trim(),
                                    //zip:                d['gsx$zip']['$t'].trim(),
                                    ,amount:            parse_currency(d['gsx$amount']['$t'].trim())
                                    ,dateofreceipt:      d['gsx$dateofreceipt']['$t'].trim()
                                    //key:                d['gsx$key']['$t'].trim()
                                
                                 }); // end push 
                                csv_parsed.push ({
                                    ID:                 d['gsx$ss']['$t'].trim()
                                    ,contractorname:     '"' + d['gsx$contractorname']['$t'].trim() + '"'
                                    ,committeename:      '"' + d['gsx$committeename']['$t'].trim() + '"'
                                    ,candidatename:      '"' + d['gsx$candidatename']['$t'].trim() + '"'
                                    //,contributorname:    d['gsx$contributorname']['$t'].trim()
                                    //address:            d['gsx$address']['$t'].trim(),
                                    //city:               d['gsx$city']['$t'].trim(),
                                    //state:              d['gsx$state']['$t'].trim(),
                                    //zip:                d['gsx$zip']['$t'].trim(),
                                    ,amount:            parse_currency(d['gsx$amount']['$t'].trim())
                                    ,dateofreceipt:      d['gsx$dateofreceipt']['$t'].trim()
                                    //key:                d['gsx$key']['$t'].trim()
                                
                                 }); // end push 
                            }
                         });
                        converter.json2csv(csv_parsed, json2csvCallback);
                        $contributions = parsed;
                        return parsed;
                    }
                }
        ,'output': 'contributions.min.json'
    }
    ,{
        'key': '1dxASXgh03oZs59Py628BRRWWB_Q3VtPGgFVBDNBiySU'
        ,'callback': function (err, data) {
                        var  parsed = [];
                        data.feed.entry.forEach (function (d) {
                            if (d['gsx$candidatename']['$t'].trim().length > 0 ) { 
                                parsed.push ({
                                     candidatename: d['gsx$candidatename']['$t'].trim()
                                    ,committeename: d['gsx$committeename']['$t'].trim()
                                    ,total: parse_currency(d['gsx$totalfundraising']['$t'].trim())
                                });
                            }
                        });
                        return parsed;
                    }
        ,'output': 'committees.min.json'            

    }
    ,{
        'key': '1IZIBuSPw2tDORWhKsvdr58E4l2VwKliewKwFBNc9P2I'
        ,'callback': function (err, data) {
                        var  parsed = [];
                        data.feed.entry.forEach (function (d) {
                            parsed.push ({
                                 candidatename: d['gsx$candidatename']['$t'].trim()
                                ,position1: d['gsx$position1']['$t'].trim()
                                ,startdate1:     d['gsx$startdate1']['$t'].trim() === '' ? '' :  new Date(d['gsx$startdate1']['$t'].trim())
                                ,enddate1:   d['gsx$enddate1']['$t'].trim() === '' ? '' :  new Date(d['gsx$enddate1']['$t'].trim())
                                ,position2:      d['gsx$position2']['$t'].trim()
                                ,startdate2: d['gsx$startdate2']['$t'].trim() === '' ? '' :  new Date(d['gsx$startdate2']['$t'].trim())
                                ,enddate2:   d['gsx$enddate2']['$t'].trim() === '' ? '' :  new Date(d['gsx$enddate2']['$t'].trim())
                            });
                        }); 
                        $incumbents = parsed;
                        return parsed;                      
                    }
        ,'output': 'incumbents.min.json'
    }
    ,{
        'key': '129eAw8UseLnA1H1vDUAXK4E5DYDE_9yj0trLV0AVVEQ'
        ,'callback' : function (err, data) {
                        var  parsed = [];
                        var contractor_industry = {};
                        data.feed.entry.forEach (function (d) {

                            parsed.push ({
                                 contractor: d['gsx$contractor1']['$t'].trim()
                                ,contractor2: d['gsx$contractor2']['$t'].trim()
                                ,contractor3: d['gsx$contractor3']['$t'].trim()
                                ,contractno: d['gsx$contractno']['$t'].trim()
                                ,industry:   d['gsx$industry']['$t'].trim()
                                ,amount:     parse_currency( d['gsx$dollaramount']['$t'].trim())
                                ,date:        new Date( d['gsx$dateoflegislativememo']['$t'].trim())
                                ,pdflink:    d['gsx$pdflink']['$t'].trim()
                                ,ceilingcontract: d['gsx$ceilingcontract']['$t'].trim().toLowerCase() 
                                //,notes:      d['gsx$notes']['$t'].trim()
                                //,legislationsummary: d['gsx$legislationsummary']['$t'].trim()     
                            });
                        });
                        $contracts = parsed;
                        $contracts.forEach(function (e){
                            $contractors.push(e.contractor);
                            $contractors.push(e.contractor2);
                            $contractors.push(e.contractor3);

                        });

                        return parsed;
                    }
        ,'output': 'contracts.min.json'
    }   
];

gulp.task('dl', function() {
    $contractors = [];
    $contributions = [];
    $incumbents = [];
    $contracts = [];


    dl_stuff.forEach( function (d) {
        getJsonFromJsonP(base_url.split('{{KEY}}').join(d.key), function (err, data) {
            require('fs').writeFile(d.output, JSON.stringify(LZW.compress(JSON.stringify(d.callback(err,data)))));
            
            if ( $incumbents.length > 0  && $contracts.length > 0 && $contributions.length > 0 ){
                console.log('blah');
               
                    
                   // var that = $rootScope;

                     incumbent_set = d3.set();
                        var rundown = [];
                        
                            // for each incumbent in spreadsheet
                            $incumbents.forEach(function (d){
                                
                                // if startdate1 and enddate1 for office are valid dates (at least one office has been held)
                                if (d.startdate1 instanceof Date && d.enddate1 instanceof Date ) {
                                    var range = [];
                                    var  in_range       = d3.set()
                                        ,in_range_2     = d3.set()
                                        ,year           = 31556926 * 1000 // year in milliseconds
                                        ;
                                    
                                    //$scope.resetCf();
                                    //cf.getCandidates().filter(d.candidatename);
                                    
                                    var contributions = $contributions.filter(function(e){return d.candidatename == e.candidatename;});

                                    range.push([d.startdate1, d.enddate1]);
                                    
                                    if (d.startdate2 instanceof Date && d.enddate2 instanceof Date) {
                                        range.push([d.startdate2, d.enddate2]);
                                    }
                                    var _contracts = [];
                                    var contracts = [];
                                    var contract_set = d3.set();
                                    var contractor_set = d3.set();
                                    
                                    // get contracts during incumbent's tenure
                                    range.forEach(function(e){
                                        $contracts.forEach(function(_d){
                                            if( + _d.date >= + e[0] && + _d.date <= + e[1] && ! _contracts.some(function(f){_d.contractno == f.contractno})){
                                                //_contracts.push(_d);
                                                
                                                contractor_set.add(_d.contractor);
                                                contractor_set.add(_d.contractor2);
                                                contractor_set.add(_d.contractor3);
                                            }
                                        });
                                    
                                    }); 

                                    // add contracts for known contractors
                                    for(var i = 0; i < $contractors.length; i+=3){
                                        if( contractor_set.has($contractors[i])){
                                            contract_set.add(i/3);
                                        }
                                        if( contractor_set.has($contractors[i+1])){
                                            contract_set.add(i/3);
                                        }
                                        if( contractor_set.has($contractors[i+2])){
                                            contract_set.add(i/3);
                                        }
                                    }

                                    contracts = contract_set.values().map(function(e){return $contracts[e];})

                                    //contracts = _contracts;

                                    // for each contract in the list
                                    contracts.forEach( function (_d) {
                                        
                                        // date of the contract
                                        var _date = +  _d.date ; 

                                        // find qualifying contributions to incumbent
                                        contributions.forEach(function ( _e, index ) {
                                            
                                            // return if contractorname is not in one of the 3 contractor columns of the spreadsheet
                                            if ([_d.contractor, _d.contractor2, _d.contractor3].indexOf(_e.contractorname) < 0 ){
                                                return;
                                            }

                                            // date of the contribution
                                            var date = + _e.dateofreceipt;
                                            
                                            // date of receipt within +/- 1 year of date of the contract -- add to in_range
                                            if ( date  >= ( _date - year ) && date  <= ( _date + year ) ){
                                                in_range.add(index);
                                            } 
                                            // date of receipt within +/- 2 years of date of the contract
                                            if ( date  >= ( _date - year * 2 ) && date  <= ( _date + year * 2 ) ){
                                                in_range_2.add(index);
                                            }
                                        });

                                    });
                                                            // total of all contributions on or after 1/1/2005
                                    var  total              = d3.sum(contributions, function(e){ return e.amount;}) 
                                        ,contributions_1    = in_range.values().map(function (e){return contributions[e];})
                                        ,_in_range_1        = contributions_1.length                            
                                        ,total_1            = d3.sum(contributions_1, function(_e){return _e.amount;})
                                        ,contributions_2    = in_range_2.values().map(function (e){return contributions[e];}) 
                                        ,_in_range_2        = contributions_2.length
                                        ,total_2            = d3.sum(contributions_2, function(_e){return _e.amount;})
                                        ;
                                    
                                    if ( total > 0 ) {
                                        rundown.push({
                                            'incumbent'             : d.candidatename
                                            ,'_in_range'             : in_range  //indices of contributions in range
                                            ,'_in_range_2'           : in_range_2
                                            ,"contracts"             : contracts  // all qualifying contracts
                                            ,"$total"                : total
                                            ,"total"                 : contributions.length
                                            ,"percent_in_1"          : total_1 / total
                                            ,"in_range_1"            : _in_range_1
                                            ,"total_in_range_1"      : total_1
                                            ,"percent_in_2"          : total_2 / total 
                                            ,"in_range_2"            : _in_range_2
                                            ,"total_in_range_2"      : total_2
                                        });
                                    
                                        in_range.values().map(function (e){return contributions[e];}).forEach( function(_d) {
                                            incumbent_set.add(_d.ID);
                                            
                                        });
                                    }
                                }   
                            });

                        // rundown.sort(that.compare('percent_in_1'));
                        console.log(rundown);
            }
        });
    });
 });


var json2csvCallback = function (err, csv) {
    if (err) throw err;
    //console.log(csv);
    require('fs').writeFile('data.csv', csv);
};


// This is an object which defines paths for the styles.
// Can add paths for javascript or images for example
// The folder, files to look for and destination are all required for sass
var paths = {

    styles: {
        src: './bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap',
        files: './bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap/**/*.scss',
        dest: '.'
    }

}

// A display error function, to format and make custom errors more uniform
// Could be combined with gulp-util or npm colors for nicer output
var displayError = function(error) {

    // Initial building up of the error
    var errorString = '[' + error.plugin + ']';
    errorString += ' ' + error.message.replace("\n",''); // Removes new line at the end

    // If the error contains the filename or line number add it to the string
    if(error.fileName)
        errorString += ' in ' + error.fileName;

    if(error.lineNumber)
        errorString += ' on line ' + error.lineNumber;

    // This will output an error like the following:
    // [gulp-sass] error message in file_name on line 1
    console.error(errorString);
}

// Setting up the sass task
gulp.task('sass', function (){
    // Taking the path from the above object
    gulp.src(paths.styles.files)
    // Sass options - make the output compressed and add the source map
    // Also pull the include path from the paths object
    .pipe(sass({
        outputStyle: 'compressed',
        sourceComments: 'map',
        includePaths : [paths.styles.src]
    }))
    // If there is an error, don't stop compiling but use the custom displayError function
    .on('error', function(err){
        displayError(err);
    })
    // Pass the compiled sass through the prefixer with defined 
    .pipe(prefix(
        'last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'
    ))
    // Funally put the compiled sass into a css file
    .pipe(gulp.dest(paths.styles.dest))
});

var jsSources = [
     'bower_components/d3/d3.min.js'
	,'bower_components/angular/angular.min.js'
    ,"bower_components/angular-touch/angular-touch.min.js"
	,'bower_components/angular-route/angular-route.min.js'
    ,"bower_components/angular-soundmanager2/dist/angular-soundmanager2.js"
    ,'bower_components/angular-sanitize/angular-sanitize.min.js'
    ,'bower_components/angular-ui-select/dist/select.min.js'
    ,'bower_components/crossfilter/crossfilter.min.js'
    //,'bower_components/ng-crossfilter/dist/ng-crossfilter.min.js'
    ,'bower_components/ng-device-detector/ng-device-detector.js'
	,'bower_components/angular-loading-bar/build/loading-bar.min.js'
    ,"bower_components/ng-device-detector/ng-device-detector.js"
	,'bower_components/d3-tip/index.js'
	,'js/main.js'
    ,'js/ie8.js'
    ,'js/LZW.js'
	,'js/factory.js'
	,'js/controller.js'
	,'js/route.js'
	,'js/directives/*.js'
];

var prod_src = ['js/app.prod.js'].concat(jsSources);
var stage_src = ['js/app.stage.js'].concat(jsSources);
var local_src = ['js/app.local.js'].concat(jsSources);



gulp.task('js', function () {
	gulp.src(prod_src)
		.pipe(jsmin())
		.pipe(concat('script.prod.js'))
		.pipe(gulp.dest('.'));
    /*
    gulp.src(stage_src)
        .pipe(jsmin())
        //.pipe(concat('script.stage.js'))
        .pipe(gulp.dest('./build/stage/js'));
    */
    gulp.src(local_src)
        .pipe(jsmin())
        .pipe(concat('script.local.js'))
        .pipe(gulp.dest('.'));

});
var cssSrcs = [
    'bower_components/bootstrap/dist/css/bootstrap.min.css'
    ,'bower_components/angular-ui-select/dist/select.min.css'
    ,'bower_components/angular-loading-bar/build/loading-bar.min.css'
    ,'css/style.css'
    ];

gulp.task('css', function() {
  gulp.src(cssSrcs)
    .pipe(minifyCSS({keepBreaks:false}))
    .pipe(gulp.dest('.'))
});


gulp.task('watch', function() {
  gulp.watch(['js/*.js', 'js/directives/*.js'], ['js']);
});

gulp.task('default', ['watch'], function() {

});